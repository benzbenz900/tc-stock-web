-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 14, 2019 at 09:21 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tckck_tc`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name_th` varchar(255) DEFAULT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_cn` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name_th`, `name_en`, `name_cn`) VALUES
(2, 'Test', 'Test', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `openbill`
--

CREATE TABLE `openbill` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `nameonbill` varchar(255) DEFAULT NULL,
  `totalprice` double(10,2) DEFAULT NULL,
  `itempiece` smallint(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `emailuser` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `contactname` varchar(255) DEFAULT NULL,
  `taxidnumber` varchar(255) DEFAULT NULL,
  `openbilldate` datetime DEFAULT CURRENT_TIMESTAMP,
  `status_pay` enum('pending','cancel','is_pay') NOT NULL DEFAULT 'pending',
  `pay_date` datetime DEFAULT NULL,
  `proof_payment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `openbill`
--

INSERT INTO `openbill` (`id`, `userid`, `nameonbill`, `totalprice`, `itempiece`, `address`, `emailuser`, `telephone`, `contactname`, `taxidnumber`, `openbilldate`, `status_pay`, `pay_date`, `proof_payment`) VALUES
(1, 1, 'test', NULL, NULL, 'stdfsdf', 'dfsdf', 'sdfsd', 'sdfs', 'sdfsd', '2019-03-02 00:00:00', 'pending', NULL, NULL),
(2, 5, 'gfhfgh', NULL, NULL, 'fghfghfg', 'fdgdfg', 'dfgdfg', 'dfgdfg', '1263123', '2019-03-04 00:00:00', 'pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `openbill_detail`
--

CREATE TABLE `openbill_detail` (
  `id` int(11) NOT NULL,
  `id_bill` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `qty_piece` int(11) DEFAULT NULL,
  `price_piece` double(10,2) DEFAULT NULL,
  `total_price` double(10,2) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `barcode_qr` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `openbill_detail`
--

INSERT INTO `openbill_detail` (`id`, `id_bill`, `id_product`, `qty_piece`, `price_piece`, `total_price`, `store_id`, `barcode_qr`) VALUES
(1, 1, 2, 5, 50.00, 250.00, NULL, NULL),
(2, 2, 2, 50, 400.00, 4000.00, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `productlist`
--

CREATE TABLE `productlist` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `storeid` tinyint(4) DEFAULT NULL,
  `qtytotal` int(11) DEFAULT '0',
  `dateadd` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` datetime DEFAULT NULL,
  `barcode_qr` varchar(255) DEFAULT NULL,
  `category` tinyint(4) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `detail` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productlist`
--

INSERT INTO `productlist` (`id`, `name`, `storeid`, `qtytotal`, `dateadd`, `date_update`, `barcode_qr`, `category`, `image`, `detail`) VALUES
(2, 'TestProduct', 2, 0, '2019-03-02 06:30:12', '2019-03-04 09:44:02', '1234567890123', 2, 'iuvp5o9opfwo8cggg4.jpg', '<p>TestProduct</p>');

-- --------------------------------------------------------

--
-- Table structure for table `storehub`
--

CREATE TABLE `storehub` (
  `id` int(11) NOT NULL,
  `name_th` varchar(255) DEFAULT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_cn` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storehub`
--

INSERT INTO `storehub` (`id`, `name_th`, `name_en`, `name_cn`) VALUES
(2, 'TestStock', 'TestStock', 'TestStock');

-- --------------------------------------------------------

--
-- Table structure for table `storelocation`
--

CREATE TABLE `storelocation` (
  `id` int(11) NOT NULL,
  `id_storehub` smallint(6) DEFAULT NULL COMMENT 'ID ตลังสินค้า',
  `name_storelocation` varchar(255) DEFAULT NULL COMMENT 'ชื้อคลังภายใน',
  `sku_storelocation` varchar(255) DEFAULT NULL COMMENT 'รหัสคลังภายใน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storelocation`
--

INSERT INTO `storelocation` (`id`, `id_storehub`, `name_storelocation`, `sku_storelocation`) VALUES
(1, 2, 'TestStockLocation01', 'LO123456'),
(2, 2, 'TestStockLocation02', 'LO123457'),
(3, 2, 'TestStockLocation03', 'LO123458'),
(4, 2, 'TestStockLocation04', 'LO987654321'),
(5, 2, 'dsfsdf', '123123');

-- --------------------------------------------------------

--
-- Table structure for table `user_system`
--

CREATE TABLE `user_system` (
  `id` smallint(6) NOT NULL,
  `username` varchar(255) DEFAULT NULL COMMENT 'ชื่อผู้ใช้ระบบ',
  `password` varchar(255) DEFAULT NULL COMMENT 'รหัสผ่าน',
  `user_level` enum('superadmin','adminbill','store','agent','block') NOT NULL DEFAULT 'block' COMMENT 'ระดับการเข้าถึง',
  `store_id` tinyint(4) DEFAULT NULL COMMENT 'ดูแลคลังสินค้าอะไร',
  `create_by` smallint(6) DEFAULT NULL COMMENT 'สร้างโดยใคร',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'สร้างเมื่อวันที่',
  `lock_user` enum('access','lock') NOT NULL DEFAULT 'access' COMMENT 'ปิดผู้ใช้งานหรือไม่'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ตารางผู้ใช้งานระบบ';

--
-- Dumping data for table `user_system`
--

INSERT INTO `user_system` (`id`, `username`, `password`, `user_level`, `store_id`, `create_by`, `create_date`, `lock_user`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'superadmin', NULL, NULL, '2019-02-25 08:21:21', 'access'),
(3, 'adminbill', '7a1a49d7ba6feb6ef4186c74ef8b154f', 'adminbill', NULL, 1, '2019-02-27 08:37:35', 'access'),
(4, 'store', '8cd892b7b97ef9489ae4479d3f4ef0fc', 'store', 2, 1, '2019-02-27 08:37:49', 'access'),
(5, 'agent', 'b33aed8f3134996703dc39f9a7c95783', 'agent', NULL, 1, '2019-02-27 08:38:07', 'access');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `openbill`
--
ALTER TABLE `openbill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `status_pay` (`status_pay`),
  ADD KEY `pay_date` (`pay_date`);

--
-- Indexes for table `openbill_detail`
--
ALTER TABLE `openbill_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_bill` (`id_bill`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `store_id` (`store_id`);

--
-- Indexes for table `productlist`
--
ALTER TABLE `productlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `storeid` (`storeid`);

--
-- Indexes for table `storehub`
--
ALTER TABLE `storehub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storelocation`
--
ALTER TABLE `storelocation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_storehub` (`id_storehub`),
  ADD KEY `sku_storelocation` (`sku_storelocation`);
ALTER TABLE `storelocation` ADD FULLTEXT KEY `name_storelocation` (`name_storelocation`);

--
-- Indexes for table `user_system`
--
ALTER TABLE `user_system`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `openbill`
--
ALTER TABLE `openbill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `openbill_detail`
--
ALTER TABLE `openbill_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `productlist`
--
ALTER TABLE `productlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `storehub`
--
ALTER TABLE `storehub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `storelocation`
--
ALTER TABLE `storelocation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_system`
--
ALTER TABLE `user_system`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
