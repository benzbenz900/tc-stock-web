{if !isset($title) }
{$title="lnwPHP"}
{/if}

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<title>{$title}</title>
	<link rel="stylesheet" href="{$BASE_COMPONENT}css/lnwphp.css">
	<script type="text/javascript" src="{$BASE_COMPONENT}js/jquery-3.3.1.min.js"></script>
	<script src="{$BASE_COMPONENT}js/sweetalert.min.js"></script>
	<script type="text/javascript">
		var BASE_URL = '{$BASE_URL}';
		var BASE_COMPONENT = '{$BASE_COMPONENT}';
		var LOGIN_PAGE = true;
		if (localStorage.lang) {
			
		} else {
			localStorage.lang = 'en';
		}
	</script>
	<script src="{$BASE_COMPONENT}js/config.js"></script>
</head>
<body>

	<div class="box" action="#" method="post">
		<img src="https://www.kroko-cosmetics.com/assets/img/logotran+.png" style="width: 100%;">
		<h1>Login</h1>
		<input type="text" name="new-username" placeholder="Username" autocomplete="new-username">
		<input type="password" name="new-password" placeholder="Password" autocomplete="new-password">
		<input type="button" name="login" value="Login">
	</div>

	<script src="{$BASE_COMPONENT}js/login.js"></script>
</body>
</html>
