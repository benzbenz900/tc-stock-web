<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">นำสินค้าออกคลังภายใน! ที่จะนำออก</h4>
  <p class="mb-0">หากนำสินค้าออกผิด กรุณา ใช้การ "นำเข้า" สินค้า ในการยกเลิก สินค้าที่นำออกผิด</p>
</div>

<hr>
<h2>นำออกสินค้า LOT:123456</h2>
<form id="inputform" class="form-inline" action="" method="post">
  <div class="form-group mb-2">
    <label for="inputBarcode" class="sr-only">รหัสสินค้า</label>
    <input type="text" class="form-control" id="inputBarcode" placeholder="รหัสสินค้า" autofocus="" data-index="1">
  </div>
   <div class="form-group mx-sm-3 mb-2">
    <label for="inputQTY" class="sr-only">QTY</label>
    <input type="text" class="form-control" id="inputQTY" placeholder="QTY" data-index="2" value="1">
  </div>
  <input type="submit" class="btn btn-primary mb-2" value="เพิ่มใน LOT" data-index="3">
</form>
<hr>

<h2>รายการนำออก</h2>
<input type="submit" class="btn btn-danger mb-2" value="บันทึก นำออก">
<table class="table">
  <thead class="bg-danger text-white">
    <tr>
      <th scope="col">#</th>
      <th scope="col">รหัสสินค้า</th>
      <th scope="col">ชื่อสินค้า</th>
      <th scope="col">จำนวน</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>QR123456789</td>
      <td>หม้อหุงข้าว Otto</td>
      <td>15 ea</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>BARCODE1234</td>
      <td>โฟมล้างหน้า</td>
      <td>100 ea</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>QR987654321</td>
      <td>แผ่นแปะสดือ</td>
      <td>500 ea</td>
    </tr>
  </tbody>
</table>
<input type="submit" class="btn btn-danger mb-2" value="บันทึก นำออก">


<script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

  $('#inputHub').on('change',function(){
    window.location.href = '{$BASE_URL}access/index/storein.html/?stocklocation='+$('#inputHub').val();
  });
  $('#inputform').on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        indextonext = (index + 1).toString();
        if(indextonext == 3){
        $('[data-index="' + indextonext + '"]').click();
      }else{
         $('[data-index="' + indextonext + '"]').focus();
      }
    }
});
</script>