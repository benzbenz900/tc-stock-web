{if !isset($title) }
{$title="lnwPHP"}
{/if}

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{$title}</title>
  <script type="text/javascript" src="{$BASE_COMPONENT}js/jquery-3.3.1.min.js"></script>
  <link href="{$BASE_COMPONENT}css/all.min.css" rel="stylesheet" type="text/css">
  <link href="{$BASE_COMPONENT}css/lnwphp.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="{$BASE_COMPONENT}css/sb-admin-2.min.css" rel="stylesheet">

  <script type="text/javascript">
    var BASE_URL = '{$BASE_URL}';
    var BASE_COMPONENT = '{$BASE_COMPONENT}';
    if (localStorage.lang) {

    } else {
      localStorage.lang = 'en';
    }
  </script>
  <script src="{$BASE_COMPONENT}js/config.js"></script>
</head>

<body id="page-top">
  <div id="wrapper">
    {if $smarty.session.user.user_level == 'superadmin'}
    {assign var="bggradient" value="bg-gradient-primary"}
    {elseif $smarty.session.user.user_level == 'adminbill'}
    {assign var="bggradient" value="bg-gradient-success"}
    {elseif $smarty.session.user.user_level == 'store'}
    {assign var="bggradient" value="bg-gradient-info"}
    {elseif $smarty.session.user.user_level == 'agent'}
    {assign var="bggradient" value="bg-gradient-danger"}
    {else}
    {assign var="bggradient" value="bg-gradient-warning"}
    {/if}

    <ul class="navbar-nav {$bggradient} sidebar sidebar-dark accordion" id="accordionSidebar">


      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text ml-3">{$smarty.session.user.user_level|ucfirst} <sup>8</sup></div>
      </a>

      <hr class="sidebar-divider my-0">
      {include file="sectionMenu/admin.tpl"}

      

    </ul>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <h1 class="m_title">Login As {$smarty.session.user.user_level|ucfirst}</h1>
          </div>

          <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{$smarty.session.user.username}</span>
                <img class="img-profile rounded-circle" src="https://via.placeholder.com/60">
              </a>

              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>

        <div class="container-fluid">
          {if $loadTable == 'filename'}

          {include file="sectionLayout/{$pageview}_layout.tpl"}

          {else}

          {$loadTable}

          {/if}
        </div>


      </div>

      <footer class="sticky-footer bg-white" style="position: relative;">
        <div class="kroko_footer"></div>
        <div class="container my-auto">

          <div class="copyright text-center my-auto">
            <span>Copyright &copy; cii3.net 2019</span>
          </div>
        </div>
      </footer>

    </div>


  </div>

  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ต้องการออกจากระบบ ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">เลือก ออกจากระบบ จากนั้นบัญชีของคุณจะออกจากระบบทันที</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">ยกเลิก</button>
          <a class="btn btn-primary" href="logout.html" onclick="localStorage.clear()">ออกจากระบบ</a>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript" src="{$BASE_COMPONENT}js/bootstrap.bundle.min.js"></script>


</body>

</html>
