var AccessCheck = function(){
  if(typeof(localStorage.usercheck) !== "undefined" && typeof(localStorage.userid) !== "undefined" && typeof(localStorage.usertoken) !== "undefined"){
    var form = new FormData();
    form.append("usertoken", localStorage.usertoken);
    form.append("usercheck", localStorage.usercheck);
    form.append("id", localStorage.userid);

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": BASE_URL+"rest/postCheckAccess/",
      "method": "POST",
      "headers": {
        "cache-control": "no-cache",
        "Postman-Token": "96c118a2-531e-4dce-89e9-c14cfe8fd020"
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form
    }

    $.ajax(settings).done(function (response) {
      response = JSON.parse(response);
      if(response.getLogin == true){

        if(typeof(LOGIN_PAGE) !== "undefined"){
          if(typeof(localStorage.usercheck) === "undefined"){
            localStorage.setItem("looplogin", 1);
          }else{
            localStorage.setItem("looplogin", parseInt(localStorage.looplogin)+1);
          }
          if(parseInt(localStorage.looplogin) > 5){
           localStorage.clear();
         }
         window.location.replace(BASE_URL+"access/index/");
       }

     }else{

      localStorage.clear();
      window.location.replace(BASE_URL+"access/index/logout.html");

    }
  });
  }else{
    localStorage.clear();

    if(typeof(LOGIN_PAGE) === "undefined"){
      window.location.replace(BASE_URL+"access/index/logout.html");
    }

  }
}

AccessCheck();