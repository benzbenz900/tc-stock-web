$('input[name=new-password]').keyup(function(e){
	if(e.keyCode == 13)
	{
		login();
	}
});
$('input[name=login]').on('click',function(){
	login();
});

var login = function(){
	var form = new FormData();
	form.append("username", $('input[name=new-username]').val());
	form.append("password", $('input[name=new-password]').val());

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": BASE_URL+"rest/postLogin/",
		"method": "POST",
		"headers": {
			"cache-control": "no-cache",
			"Postman-Token": "2e8eac92-454b-498d-8c7a-0ec97a5010d0"
		},
		"processData": false,
		"contentType": false,
		"mimeType": "multipart/form-data",
		"data": form
	}

	$.ajax(settings).done(function (response) {
		response = JSON.parse(response);
		if(response.getLogin === null){
			swal(LOGIN_STOP, LOGIN_USER_PASS_WRONG, "error");
		}else{
			if(response.getLogin.lock_user == "access"){
				if (typeof(Storage) !== "undefined") {
					localStorage.setItem("userid", response.getLogin.id);
					localStorage.setItem("usercheck", response.session);
					localStorage.setItem("usertoken", btoa(response.getLogin.password+response.getLogin.user_level+response.getLogin.lock_user));
					swal(LOGIN_SUCCESS, LOGIN_SUCCESS_TEXT, "success")
					.then((value) => {
						 window.location.replace(BASE_URL+"access/index/");
					});
				} else {
					swal(LOGIN_DISABLE, LOGIN_DISABLE_STORAGE, "error");
				}
				
			}else{
				swal(LOGIN_DISABLE, LOGIN_DISABLE_TEXT, "error");
			}
		}
	});
}