var LOGIN_STOP = "ถูกห้าม!";
var LOGIN_USER_PASS_WRONG = "ชื่อผู้ใช้หรือรหัสไม่ถูกต้อง";
var LOGIN_DISABLE = "ถูกปิดการใช้งาน";
var LOGIN_DISABLE_TEXT = "ผู้ใช้นี้ถูกปิดการใช้งานไปแล้ว";
var LOGIN_DISABLE_STORAGE = "ไม่สนับสนุน Web Storage";
var LOGIN_SUCCESS = "สำเร็จ";
var LOGIN_SUCCESS_TEXT = "เข้าสู่ระบบเรียบร้อยแล้ว";
var LOGIN_WORKER = "Web Worker";
var LOGIN_WORKER_TEXT = "ไม่สนับสนุน Web Worker";