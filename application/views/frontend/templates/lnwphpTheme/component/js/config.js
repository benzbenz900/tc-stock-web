var loadScript = [
BASE_COMPONENT+'js/lang.{lang}.js',
BASE_COMPONENT+'js/check_access.js',
]

function loadjs(item) {
	var script = document.createElement('script');
	script.onload = function () {

	};
	script.src = item.replace("{lang}", localStorage.getItem("lang"));
	document.getElementsByTagName('head')[0].appendChild(script);
}

loadScript.forEach(loadjs);