<div class="navbarmenu">
{if $smarty.session.user.user_level == 'superadmin'}
<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages1">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการ</span>
  </a>
  <div id="collapsePages1" class="collapse" aria-labelledby="headingPages1" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{$BASE_URL}access/index/category.html">หมวดหมู่สินค้า</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/storehub.html">คลังสินค้าใหญ่</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/storelocation.html">คลังสินค้าภายใน</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/productlist.html">รายการสินค้า</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/useraccess.html">ผู้ใช้งานระบบ</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/billlist.html">รายการบิล</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/summary.html">สรุปยอดขาย</a>
    </div>
  </div>
</li>

{/if}
{if $smarty.session.user.user_level == 'adminbill' || $smarty.session.user.user_level == 'superadmin'}

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages3" aria-expanded="true" aria-controls="collapsePages3">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการบิล</span>
  </a>
  <div id="collapsePages3" class="collapse" aria-labelledby="headingPages3" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
     <a class="collapse-item" href="{$BASE_URL}access/index/openbill.html">เปิดบิลสินค้า</a>
     <a class="collapse-item" href="{$BASE_URL}access/index/customlist.html">รายนามลูกค้า</a>
     {* <a class="collapse-item" href="{$BASE_URL}access/index/billlist.html">รายการบิล</a> *}
     <a class="collapse-item" href="{$BASE_URL}access/index/summary.html">สรุปยอดขาย</a>
   </div>
 </div>
</li>

{/if}
{if $smarty.session.user.user_level == 'store' || $smarty.session.user.user_level == 'superadmin'}

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages2" aria-expanded="true" aria-controls="collapsePages2">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการคลัง</span>
  </a>
  <div id="collapsePages2" class="collapse" aria-labelledby="headingPages2" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{$BASE_URL}access/index/storehub.html">คลังสินค้าใหญ่</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/stroeproductlist.html">สินค้าในคลัง</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/storebill.html">บิลของคลัง</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/storelocation.html">คลังสินค้าภายใน</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/storein.html">นำสินค้าเข้า</a>
      <a class="collapse-item" href="{$BASE_URL}access/index/storeout.html">นำสินค้าออก</a>
    </div>
  </div>
</li>

{/if}
{if $smarty.session.user.user_level == 'agent' || $smarty.session.user.user_level == 'superadmin'}

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages4" aria-expanded="true" aria-controls="collapsePages4">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการตัวแทนจำหน่าย</span>
  </a>
  <div id="collapsePages4" class="collapse" aria-labelledby="headingPages4" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
     <a class="collapse-item" href="{$BASE_URL}access/index/productlist.html">รายการสินค้า</a>
     <a class="collapse-item" href="{$BASE_URL}access/index/orderproduct.html">สั่งชื้อสินค้า</a>
     <a class="collapse-item" href="{$BASE_URL}access/index/purchased.html">สินค้าที่เคย สั่งซื้อ</a>
     <a class="collapse-item" href="{$BASE_URL}access/index/mybilllist.html">รายการบิลสินค้า</a>
   </div>
 </div>
</li>

{/if}

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
    <i class="fas fa-fw fa-folder"></i>
    <span>ออกจากระบบ</span>
  </a>
</li>

</div>