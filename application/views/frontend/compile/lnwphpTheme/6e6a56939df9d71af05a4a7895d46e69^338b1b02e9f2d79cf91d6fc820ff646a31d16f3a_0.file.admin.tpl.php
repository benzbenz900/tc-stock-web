<?php
/* <!-- PIPe MCV & Smarty HHVM By lnwPHP.in.th & cii3.net -->  */


/* Modify For PIPe MVC HHVM By Smarty version 3.1.33, created on 2019-03-02 05:46:44
  from '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionMenu/admin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c7a18c493dc10_76730347',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '338b1b02e9f2d79cf91d6fc820ff646a31d16f3a' => 
    array (
      0 => '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionMenu/admin.tpl',
      1 => 1551256607,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c7a18c493dc10_76730347 (Smarty_Internal_Template $_smarty_tpl) {
if ($_SESSION['user']['user_level'] == 'superadmin') {?>

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages1">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการ</span>
  </a>
  <div id="collapsePages1" class="collapse" aria-labelledby="headingPages1" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="category.html">หมวดหมู่สินค้า</a>
      <a class="collapse-item" href="storehub.html">คลังสินค้า</a>
      <a class="collapse-item" href="productlist.html">รายการสินค้า</a>
      <a class="collapse-item" href="useraccess.html">ผู้ใช้งานระบบ</a>
      <a class="collapse-item" href="billlist.html">รายการบิล</a>
      <a class="collapse-item" href="summary.html">สรุปยอดขาย</a>
    </div>
  </div>
</li>

<?php }
if ($_SESSION['user']['user_level'] == 'adminbill' || $_SESSION['user']['user_level'] == 'superadmin') {?>

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages3" aria-expanded="true" aria-controls="collapsePages3">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการบิล</span>
  </a>
  <div id="collapsePages3" class="collapse" aria-labelledby="headingPages3" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
     <a class="collapse-item" href="openbill.html">เปิดบิลสินค้า</a>
     <a class="collapse-item" href="customlist.html">รายนามลูกค้า</a>
     <a class="collapse-item" href="billlist.html">รายการบิล</a>
     <a class="collapse-item" href="summary.html">สรุปยอดขาย</a>
   </div>
 </div>
</li>

<?php }
if ($_SESSION['user']['user_level'] == 'store' || $_SESSION['user']['user_level'] == 'superadmin') {?>

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages2" aria-expanded="true" aria-controls="collapsePages2">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการคลัง</span>
  </a>
  <div id="collapsePages2" class="collapse" aria-labelledby="headingPages2" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="productlist.html">สินค้าในคลัง</a>
      <a class="collapse-item" href="storebill.html">บิลของคลัง</a>
    </div>
  </div>
</li>

<?php }
if ($_SESSION['user']['user_level'] == 'agent' || $_SESSION['user']['user_level'] == 'superadmin') {?>

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages4" aria-expanded="true" aria-controls="collapsePages4">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการตัวแทนจำหน่าย</span>
  </a>
  <div id="collapsePages4" class="collapse" aria-labelledby="headingPages4" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
     <a class="collapse-item" href="productlist.html">รายการสินค้า</a>
     <a class="collapse-item" href="orderproduct.html">สั่งชื้อสินค้า</a>
     <a class="collapse-item" href="purchased.html">สินค้าที่เคย สั่งซื้อ</a>
   </div>
 </div>
</li>

<?php }?>


<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
    <i class="fas fa-fw fa-folder"></i>
    <span>ออกจากระบบ</span>
  </a>
</li><?php }
}
