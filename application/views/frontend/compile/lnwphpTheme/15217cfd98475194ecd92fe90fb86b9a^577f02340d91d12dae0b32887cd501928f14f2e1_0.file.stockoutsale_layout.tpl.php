<?php
/* <!-- PIPe MCV & Smarty HHVM By lnwPHP.in.th & cii3.net -->  */


/* Modify For PIPe MVC HHVM By Smarty version 3.1.33, created on 2019-03-10 08:27:02
  from '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/stockoutsale_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c84ca56d09775_39097235',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '577f02340d91d12dae0b32887cd501928f14f2e1' => 
    array (
      0 => '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/stockoutsale_layout.tpl',
      1 => 1552206502,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c84ca56d09775_39097235 (Smarty_Internal_Template $_smarty_tpl) {
?><h2>ใบนำออกสินค้า</h2>
<form id="inputform" class="form-inline" action="" method="post">
  <div class="form-group mb-2">
    <label for="inputBarcode" class="sr-only">เลขใบนำออกสินค้า/บิล</label>
    <input type="text" class="form-control" id="inputBarcode" placeholder="เลขใบนำออกสินค้า/บิล" autofocus="" data-index="1">
  </div>
</form>
<hr>

<h2>สินค้าที่ขายให้</h2>
<form id="inputform" class="form-inline" action="" method="post">
  <div class="form-group mb-2">
    <label for="inputBarcode" class="sr-only">รหัสสินค้า</label>
    <input type="text" class="form-control" id="inputBarcode" placeholder="รหัสสินค้า" autofocus="" data-index="1">
  </div>
</form>
<hr>

<h2>รายการสินค้า</h2>
<input type="submit" class="btn btn-danger mb-2" value="บันทึก นำออก">
<table class="table">
  <thead class="bg-danger text-white">
    <tr>
      <th scope="col">#</th>
      <th scope="col">รหัสสินค้า</th>
      <th scope="col">ชื่อสินค้า</th>
      <th scope="col">คลังเก็บ</th>
      <th scope="col">จำนวน</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>QR123456789</td>
      <td>หม้อหุงข้าว Otto</td>
      <td>หม้อหุงข้าว Otto</td>
      <td>15 ea</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>BARCODE1234</td>
      <td>โฟมล้างหน้า</td>
      <td>หม้อหุงข้าว Otto</td>
      <td>100 ea</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>QR987654321</td>
      <td>แผ่นแปะสดือ</td>
      <td>หม้อหุงข้าว Otto</td>
      <td>500 ea</td>
    </tr>
  </tbody>
</table>
<input type="submit" class="btn btn-danger mb-2" value="บันทึก นำออก">


<?php echo '<script'; ?>
 type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

  $('#inputHub').on('change',function(){
    window.location.href = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
access/index/storein.html/?stocklocation='+$('#inputHub').val();
  });
  $('#inputform').on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        indextonext = (index + 1).toString();
        if(indextonext == 3){
        $('[data-index="' + indextonext + '"]').click();
      }else{
         $('[data-index="' + indextonext + '"]').focus();
      }
    }
});
<?php echo '</script'; ?>
><?php }
}
