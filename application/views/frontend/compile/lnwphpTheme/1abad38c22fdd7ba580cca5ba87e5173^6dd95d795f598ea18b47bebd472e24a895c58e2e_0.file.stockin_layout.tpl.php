<?php
/* <!-- PIPe MCV & Smarty HHVM By lnwPHP.in.th & cii3.net -->  */


/* Modify For PIPe MVC HHVM By Smarty version 3.1.33, created on 2019-03-03 15:51:34
  from '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/stockin_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c7bf806adce07_34416655',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6dd95d795f598ea18b47bebd472e24a895c58e2e' => 
    array (
      0 => '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/stockin_layout.tpl',
      1 => 1551628342,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c7bf806adce07_34416655 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="alert alert-dismissible alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">นำสินค้าเข้าคลังภายใน!</h4>
  <p class="mb-0">หากนำสินค้าเข้าผิด กรุณา ใช้การ "นำออก" สินค้า ในการยกเลิก สินค้าที่นำเข้าผิด</p>
</div>
<h2>เลือกคลังสินค้าภายใน</h2>
<form class="form-inline" action="" method="post">
  <div class="form-group mb-2">
    <label for="inputHub" class="sr-only">คลังสินค้าภายใน</label>
    <select class="form-control" id="inputHub">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
</form>
<hr>

<h2>นำเข้าสินค้า</h2>
<form id="inputform" class="form-inline" action="" method="post">
  <div class="form-group mb-2">
    <label for="inputBarcode" class="sr-only">Barcode / QR Code</label>
    <input type="text" class="form-control" id="inputBarcode" placeholder="Scan Barcode / QR Code" autofocus="" data-index="1">
  </div>
   <div class="form-group mx-sm-3 mb-2">
    <label for="inputQTY" class="sr-only">QTY</label>
    <input type="text" class="form-control" id="inputQTY" placeholder="QTY" data-index="2" value="1">
  </div>
  <input type="submit" class="btn btn-primary mb-2" value="Confirm identity" data-index="3">
</form>
<hr>

<h2>รายการนำเข้า</h2>
<input type="submit" class="btn btn-success mb-2" value="บันทึก นำเข้า">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
<input type="submit" class="btn btn-success mb-2" value="บันทึก นำเข้า">
<?php echo '<script'; ?>
 type="text/javascript">
  $('#inputform').on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        indextonext = (index + 1).toString();
        if(indextonext == 3){
        $('[data-index="' + indextonext + '"]').click();
      }else{
         $('[data-index="' + indextonext + '"]').focus();
      }
    }
});
<?php echo '</script'; ?>
><?php }
}
