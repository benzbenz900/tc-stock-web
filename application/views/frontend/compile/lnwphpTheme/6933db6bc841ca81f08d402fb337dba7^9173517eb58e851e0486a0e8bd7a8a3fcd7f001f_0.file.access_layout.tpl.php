<?php
/* <!-- PIPe MCV & Smarty HHVM By lnwPHP.in.th & cii3.net -->  */


/* Modify For PIPe MVC HHVM By Smarty version 3.1.33, created on 2019-03-02 08:01:44
  from '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/access_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c7a3868c73a23_16710856',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9173517eb58e851e0486a0e8bd7a8a3fcd7f001f' => 
    array (
      0 => '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/access_layout.tpl',
      1 => 1551509862,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:sectionMenu/admin.tpl' => 1,
    'file:sectionLayout/".((string)$_smarty_tpl->tpl_vars[\'pageview\']->value)."_layout.tpl' => 1,
  ),
),false)) {
function content_5c7a3868c73a23_16710856 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_smarty_tpl->tpl_vars['title']->value)) {
$_smarty_tpl->_assignInScope('title', "lnwPHP");
}?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
  <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
  <link href="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
css/lnwphp.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
css/sb-admin-2.min.css" rel="stylesheet">

  <?php echo '<script'; ?>
 type="text/javascript">
    var BASE_URL = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
';
    var BASE_COMPONENT = '<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
';
    if (localStorage.lang) {

    } else {
      localStorage.lang = 'en';
    }
  <?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
js/config.js"><?php echo '</script'; ?>
>
</head>

<body id="page-top">
  <div id="wrapper">
    <?php if ($_SESSION['user']['user_level'] == 'superadmin') {?>
    <?php $_smarty_tpl->_assignInScope('bggradient', "bg-gradient-primary");?>
    <?php } elseif ($_SESSION['user']['user_level'] == 'adminbill') {?>
    <?php $_smarty_tpl->_assignInScope('bggradient', "bg-gradient-success");?>
    <?php } elseif ($_SESSION['user']['user_level'] == 'store') {?>
    <?php $_smarty_tpl->_assignInScope('bggradient', "bg-gradient-info");?>
    <?php } elseif ($_SESSION['user']['user_level'] == 'agent') {?>
    <?php $_smarty_tpl->_assignInScope('bggradient', "bg-gradient-danger");?>
    <?php } else { ?>
    <?php $_smarty_tpl->_assignInScope('bggradient', "bg-gradient-warning");?>
    <?php }?>

    <ul class="navbar-nav <?php echo $_smarty_tpl->tpl_vars['bggradient']->value;?>
 sidebar sidebar-dark accordion" id="accordionSidebar">


      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text ml-3"><?php echo ucfirst($_SESSION['user']['user_level']);?>
 <sup>8</sup></div>
      </a>

      <hr class="sidebar-divider my-0">
      <?php echo "<!-- lnwPHP.in.th & cii3.net -->";
$_smarty_tpl->_subTemplateRender("file:sectionMenu/admin.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      

    </ul>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <h1 class="m_title">Login As <?php echo ucfirst($_SESSION['user']['user_level']);?>
</h1>
          </div>

          <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['user']['username'];?>
</span>
                <img class="img-profile rounded-circle" src="https://via.placeholder.com/60">
              </a>

              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>

        <div class="container-fluid">
          <?php if ($_smarty_tpl->tpl_vars['loadTable']->value == 'filename') {?>

          <?php echo "<!-- lnwPHP.in.th & cii3.net -->";
$_smarty_tpl->_subTemplateRender("file:sectionLayout/".((string)$_smarty_tpl->tpl_vars['pageview']->value)."_layout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

          <?php } else { ?>

          <?php echo $_smarty_tpl->tpl_vars['loadTable']->value;?>


          <?php }?>
        </div>


      </div>

      <footer class="sticky-footer bg-white" style="position: relative;">
        <div class="kroko_footer"></div>
        <div class="container my-auto">

          <div class="copyright text-center my-auto">
            <span>Copyright &copy; cii3.net 2019</span>
          </div>
        </div>
      </footer>

    </div>


  </div>

  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ต้องการออกจากระบบ ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">เลือก ออกจากระบบ จากนั้นบัญชีของคุณจะออกจากระบบทันที</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">ยกเลิก</button>
          <a class="btn btn-primary" href="logout.html" onclick="localStorage.clear()">ออกจากระบบ</a>
        </div>
      </div>
    </div>
  </div>


  <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>


</body>

</html>
<?php }
}
