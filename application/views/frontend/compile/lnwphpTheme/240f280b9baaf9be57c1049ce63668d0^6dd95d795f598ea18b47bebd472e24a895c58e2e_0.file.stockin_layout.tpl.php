<?php
/* <!-- PIPe MCV & Smarty HHVM By lnwPHP.in.th & cii3.net -->  */


/* Modify For PIPe MVC HHVM By Smarty version 3.1.33, created on 2019-03-03 16:57:31
  from '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/stockin_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c7c077bead274_50806058',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6dd95d795f598ea18b47bebd472e24a895c58e2e' => 
    array (
      0 => '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/stockin_layout.tpl',
      1 => 1551631524,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c7c077bead274_50806058 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_GET['stocklocation'])) {
echo $_smarty_tpl->tpl_vars['storelocation']->value;?>

<hr>
<?php }
if (isset($_GET['stocklocation'])) {?>
<div class="alert alert-dismissible alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">นำสินค้าเข้าคลังภายใน! ที่จะนำเข้า</h4>
  <p class="mb-0">หากนำสินค้าเข้าผิด กรุณา ใช้การ "นำออก" สินค้า ในการยกเลิก สินค้าที่นำเข้าผิด</p>
</div>
<a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
access/index/storein.html" class="btn btn-primary mb-2"><< กลับไป เลือกคลังสินค้าภายใน</a>
<hr>
<h2>นำเข้าสินค้า LOT:123456</h2>
<form id="inputform" class="form-inline" action="" method="post">
  <div class="form-group mb-2">
    <label for="inputBarcode" class="sr-only">รหัสสินค้า</label>
    <input type="text" class="form-control" id="inputBarcode" placeholder="รหัสสินค้า" autofocus="" data-index="1">
  </div>
   <div class="form-group mx-sm-3 mb-2">
    <label for="inputQTY" class="sr-only">QTY</label>
    <input type="text" class="form-control" id="inputQTY" placeholder="QTY" data-index="2" value="1">
  </div>
  <input type="submit" class="btn btn-primary mb-2" value="เพิ่มใน LOT" data-index="3">
</form>
<hr>

<h2>รายการนำเข้า</h2>
<input type="submit" class="btn btn-success mb-2" value="บันทึก นำเข้า">
<table class="table">
  <thead class="bg-success text-white">
    <tr>
      <th scope="col">#</th>
      <th scope="col">รหัสสินค้า</th>
      <th scope="col">ชื่อสินค้า</th>
      <th scope="col">จำนวน</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>QR123456789</td>
      <td>หม้อหุงข้าว Otto</td>
      <td>15 ea</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>BARCODE1234</td>
      <td>โฟมล้างหน้า</td>
      <td>100 ea</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>QR987654321</td>
      <td>แผ่นแปะสดือ</td>
      <td>500 ea</td>
    </tr>
  </tbody>
</table>
<input type="submit" class="btn btn-success mb-2" value="บันทึก นำเข้า">

<?php }?>

<?php echo '<script'; ?>
 type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

  $('#inputHub').on('change',function(){
    window.location.href = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
access/index/storein.html/?stocklocation='+$('#inputHub').val();
  });
  $('#inputform').on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        indextonext = (index + 1).toString();
        if(indextonext == 3){
        $('[data-index="' + indextonext + '"]').click();
      }else{
         $('[data-index="' + indextonext + '"]').focus();
      }
    }
});
<?php echo '</script'; ?>
><?php }
}
