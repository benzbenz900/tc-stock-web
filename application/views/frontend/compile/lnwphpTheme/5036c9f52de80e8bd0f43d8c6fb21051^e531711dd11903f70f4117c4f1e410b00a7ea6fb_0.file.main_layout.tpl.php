<?php
/* <!-- PIPe MCV & Smarty HHVM By lnwPHP.in.th & cii3.net -->  */


/* Modify For PIPe MVC HHVM By Smarty version 3.1.33, created on 2019-03-05 02:07:12
  from '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/main_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c7dd9d0b86c59_54906898',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e531711dd11903f70f4117c4f1e410b00a7ea6fb' => 
    array (
      0 => '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionLayout/main_layout.tpl',
      1 => 1551259582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c7dd9d0b86c59_54906898 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_smarty_tpl->tpl_vars['title']->value)) {
$_smarty_tpl->_assignInScope('title', "lnwPHP");
}?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
css/lnwphp.css">
	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
js/sweetalert.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript">
		var BASE_URL = '<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
';
		var BASE_COMPONENT = '<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
';
		var LOGIN_PAGE = true;
		if (localStorage.lang) {
			
		} else {
			localStorage.lang = 'en';
		}
	<?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
js/config.js"><?php echo '</script'; ?>
>
</head>
<body>

	<div class="box" action="#" method="post">
		<img src="https://www.kroko-cosmetics.com/assets/img/logotran+.png" style="width: 100%;">
		<h1>Login</h1>
		<input type="text" name="new-username" placeholder="Username" autocomplete="new-username">
		<input type="password" name="new-password" placeholder="Password" autocomplete="new-password">
		<input type="button" name="login" value="Login">
	</div>

	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_COMPONENT']->value;?>
js/login.js"><?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
