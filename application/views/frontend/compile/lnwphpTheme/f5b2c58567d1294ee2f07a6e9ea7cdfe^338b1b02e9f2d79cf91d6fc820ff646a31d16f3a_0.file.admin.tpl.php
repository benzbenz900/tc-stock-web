<?php
/* <!-- PIPe MCV & Smarty HHVM By lnwPHP.in.th & cii3.net -->  */


/* Modify For PIPe MVC HHVM By Smarty version 3.1.33, created on 2019-02-25 17:23:46
  from '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionMenu/admin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c7424a254b0d4_00716783',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '338b1b02e9f2d79cf91d6fc820ff646a31d16f3a' => 
    array (
      0 => '/home/tckck/tc.kck.co.th/public_html/application/views/frontend/templates/lnwphpTheme/sectionMenu/admin.tpl',
      1 => 1551113869,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c7424a254b0d4_00716783 (Smarty_Internal_Template $_smarty_tpl) {
?><li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages1">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการ</span>
  </a>
  <div id="collapsePages1" class="collapse" aria-labelledby="headingPages1" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">ผู้ดูแล</h6>
      <a class="collapse-item" href="#">รายการสินค้า</a>
      <a class="collapse-item" href="#">คลังสินค้า</a>
      <a class="collapse-item" href="#">ผู้ใช้งานระบบ</a>
      <a class="collapse-item" href="#">รายการเปิดบิล</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages2" aria-expanded="true" aria-controls="collapsePages2">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการคลัง</span>
  </a>
  <div id="collapsePages2" class="collapse" aria-labelledby="headingPages2" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">คลัง</h6>
       <a class="collapse-item" href="#">เพิ่มคลังสินค้า</a>
       <a class="collapse-item" href="#">เพิ่มผู้ดูแลคลัง</a>
       <a class="collapse-item" href="#">เพิ่มสินค้าในคลัง</a>
       <a class="collapse-item" href="#">บิลของคลัง</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages3" aria-expanded="true" aria-controls="collapsePages3">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการบิล</span>
  </a>
  <div id="collapsePages3" class="collapse" aria-labelledby="headingPages3" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">บิล</h6>
      <a class="collapse-item" href="404.html">404 Page</a>
      <a class="collapse-item" href="blank.html">Blank Page</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages4" aria-expanded="true" aria-controls="collapsePages4">
    <i class="fas fa-fw fa-folder"></i>
    <span>จัดการตัวแทนจำหน่าย</span>
  </a>
  <div id="collapsePages4" class="collapse" aria-labelledby="headingPages4" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">ตัวแทนจำหน่าย</h6>
      <a class="collapse-item" href="404.html">404 Page</a>
      <a class="collapse-item" href="blank.html">Blank Page</a>
    </div>
  </div>
</li><?php }
}
