<?php
class Access extends Controller {

	function index($pageview='index.html',$action='')
	{
		$t = $this->loadView('access');

		if(isset($_SESSION['user']['id']) && is_numeric($_SESSION['user']['id']) && $pageview != 'logout.html'){

			if($pageview == 'storein.html' || $pageview == 'storeout.html' || $pageview == 'storeoutsale.html' || $pageview == 'index.html' ||	$pageview == 'summary.html' || $pageview == 'logout.html' || $pageview == ''){

				switch ($pageview) {
					case 'index.html':
					case 'summary.html':

					if($_SESSION['user']['user_level'] == 'superadmin' || $_SESSION['user']['user_level'] == 'adminbill'){

						$admin_model = $this->loadModel('xcrud/admin_model');
						$loadTable = $this->loadAdmin()->unset_view()->unset_limitlist()->hide_button('save')->hide_button('save_edit')->hide_button('save_new');
						$bill_is_pay = $admin_model->billlist($loadTable,'is_pay');

						$loadTable2 = $this->loadAdmin(true);
						$bill_pending = $admin_model->billlist($loadTable2,'pending');

						$t->set('bill_is_pay',$bill_is_pay);
						$t->set('bill_pending',$bill_pending);

						$t->set('pageview','admin');

					}elseif($_SESSION['user']['user_level'] == 'store'){
						$this->redirect('/access/index/storeoutsale.html');
					}

					$t->set('loadTable','filename');
					break;

					case 'storein.html':

					if(!isset($_GET['stocklocation'])){
						$loadTable = $this->loadAdmin()->unset_view()->unset_limitlist()->hide_button('save')->hide_button('save_edit')->hide_button('save_new');
						$model = $this->loadModel('xcrud/store_model');
						$storelocation = $model->storelocation($loadTable,true,$_SESSION['user']['store_id']);
						$t->set('storelocation',$storelocation);
					}

					$t->set('pageview','stockin');
					$t->set('loadTable','filename');

					break;

					case 'storeoutsale.html':
					$t->set('pageview','stockoutsale');
					$t->set('loadTable','filename');
					break;

					case 'storeout.html':
					$t->set('pageview','stockout');
					$t->set('loadTable','filename');
					break;
					
					default:
					$t->set('loadTable','ERROR');
					break;
				}

			}else{

				$loadTable = $this->loadAdmin()->unset_view()->unset_limitlist()->hide_button('save')->hide_button('save_edit')->hide_button('save_new');

				if($_SESSION['user']['user_level'] == 'superadmin'){


				}elseif($_SESSION['user']['user_level'] == 'adminbill'){

					$loadTable
					->unset_edit()
					->unset_view()
					->unset_remove();

				}elseif($_SESSION['user']['user_level'] == 'store'){

					$loadTable->unset_edit()
					->unset_view()
					->unset_remove();

				}elseif($_SESSION['user']['user_level'] == 'agent'){

					$loadTable->unset_add()
					->unset_edit()
					->unset_view()
					->unset_remove();

				}else{

					echo "ACCESS DENY";
					exit();

				}

				switch ($pageview) {

					case 'productlist.html':
					$model = $this->loadModel('xcrud/product_model');
					$loadTable = $model->productlist($loadTable);
					break;

					case 'storelocation.html':
					$model = $this->loadModel('xcrud/store_model');
					$loadTable = $model->storelocation($loadTable,false,$_SESSION['user']['store_id']);
					break;

					case 'stroeproductlist.html':
					$model = $this->loadModel('xcrud/product_model');
					$loadTable = $model->productlist($loadTable,$_SESSION['user']['store_id']);
					break;
					

					case 'category.html':
					$model = $this->loadModel('xcrud/category_model');
					$loadTable = $model->category($loadTable);
					break;

					case 'storehub.html':
					$model = $this->loadModel('xcrud/store_model');
					$loadTable = $model->storehub($loadTable);
					break;

					case 'useraccess.html':
					$model = $this->loadModel('xcrud/user_model');
					$loadTable = $model->useraccess($loadTable);
					break;

					case 'billlist.html':
					$model = $this->loadModel('xcrud/bill_model');
					$loadTable = $model->billlist($loadTable);
					break;

					case 'openbill.html':
					$model = $this->loadModel('xcrud/bill_model');
					$loadTable = $model->billlist($loadTable,'เปิดบิล สั่งชื้อแทน');
					break;

					case 'customlist.html':
					$model = $this->loadModel('xcrud/user_model');
					$loadTable = $model->useraccess($loadTable,'agent');
					break;

					case 'billdetail.html':
					$model = $this->loadModel('xcrud/bill_model');
					$loadTable = $model->billdetail($loadTable);
					break;

					case 'storebill.html':
					$model = $this->loadModel('xcrud/bill_model');
					$loadTable = $model->billlist($loadTable);
					break;

					default:
					$loadTable = $pageview;
					break;

				}

				if($action == 'add'){
					$t->set('loadTable',(method_exists($loadTable,'render')) ? $loadTable->render('create') : $loadTable);
				}else{
					$t->set('loadTable',(method_exists($loadTable,'render')) ? $loadTable->render() : $loadTable);
				}

			}

		}else{

			unset($_SESSION);
			session_destroy();
			$this->redirect('/');
			exit();

		}

		$t->render();
	}

}

?>
