<?php
class Rest extends Controller {
	
	function postLogin()
	{
		header("Content-type: application/json; charset=utf-8");

		$t = $this->loadView('sectionLayout/rest');

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			if(isset($_POST['username']) && isset($_POST['password'])){
				$login_model = $this->loadModel('login_model');

				$_SESSION['user'] = $login_model->getLogin(
					trim($_POST['username']),
					md5(
						trim($_POST['password'])
					)
				);

				$t->set('data_rest',
					array(
						'getLogin' => $_SESSION['user'],
						'session' => session_id()
					)
				);
			}else{
				$t->set('data_rest',
					array(
						'getLogin' => null,
					)
				);
			}

		}else{

			$t->set('data_rest',
				array(
					'method' => 'not allow',
				)
			);

		}

		$t->render();
	}

	function postCheckAccess()
	{
		header("Content-type: application/json; charset=utf-8");

		$t = $this->loadView('sectionLayout/rest');

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			if(session_id() == $_POST['usercheck'] && is_numeric($_POST['id'])){
				$login_model = $this->loadModel('login_model');
				$_user = $login_model->getLoginId(
					trim($_POST['id'])
				);

				if($_user['id'] == $_POST['id']){
					
					$token = base64_encode($_user['password'].$_user['user_level'].$_user['lock_user']);
					if($token == $_POST['usertoken']){
						$t->set('data_rest',
							array(
								'getLogin' => true,
							)
						);
					}else{
						$t->set('data_rest',
							array(
								'getLogin' => null,
							)
						);
					}
					

				}else{

					$t->set('data_rest',
						array(
							'getLogin' => null,
						)
					);

				}
				

			}else{

				$t->set('data_rest',
					array(
						'method' => null,
					)
				);

			}

		}else{

			$t->set('data_rest',
				array(
					'method' => 'not allow',
				)
			);

		}

		$t->render();
	}

}

?>
