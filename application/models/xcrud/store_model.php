<?php
class xcrud_store_model extends Model {

	public function storehub($loadTable='')
	{
		$loadTable->table('storehub');
		$loadTable->table_name('คลังสินค้าหลัง');
		$loadTable->columns('name_cn,name_en,name_th');
		$loadTable->label(
			array(
				'name_cn' => 'ชื่อจีน',
				'name_en' => 'ชื่ออังกฤษ',
				'name_th' => 'ชื่อไทย',

			)
		);

		$loadTable->order_by('id','DESC');

		return $loadTable;
	}

	public function storelocation($loadTable='',$unall=false,$storeid=false)
	{
		$loadTable->table('storelocation');
		if($unall !== false){
			$loadTable->table_name('เลือก คลังสินค้าภายใน ที่จะนำเข้า');
			$loadTable->unset_edit()->unset_view()->unset_remove()->unset_csv()->unset_print()->unset_limitlist();
			$loadTable->button('storein.html/?stocklocation={id}','นำเข้าสินค้าในคลังนี้','fas fa-plus-circle');
		}else{
			$loadTable->table_name('คลังสินค้าภายใน');
			$loadTable->button('stocklocationview.html/?stocklocation={id}','ดูสินค้าในคลังนี้','fas fa-search');
		}

		if($storeid !== false){
			if($storeid == 0 && $_SESSION['user']['user_level'] == 'superadmin'){
				$loadTable->fields('id_storehub,name_storelocation,sku_storelocation');
			}else{
				$loadTable->pass_var('id_storehub',$storeid,'create');
				$loadTable->where('id_storehub =',$storeid);
				$loadTable->fields('name_storelocation,sku_storelocation');
			}
		}

		$loadTable->relation('id_storehub','storehub','id','name_cn');
		$loadTable->columns('name_storelocation,sku_storelocation,id_storehub');
		$loadTable->label(
			array(
				'id_storehub' => 'คลังสินค้าหลัก',
				'name_storelocation' => 'ชื่อคลังภายใน',
				'sku_storelocation' => 'รหัสคลังภายใน',

			)
		);

		$loadTable->order_by('id','DESC');

		return $loadTable;
	}

}

?>
