<?php
class xcrud_admin_model extends Model {

	public function billlist($loadTable='',$is_pay='is_pay')
	{
		$loadTable->table('openbill');

		$loadTable->unset_add()
					->unset_edit()
					->unset_view()
					->unset_remove();

		if($is_pay == 'is_pay'){
			$loadTable->table_name('บิลสั่งชื้อ ที่จ่ายแล้ว');
		}else{
			$loadTable->table_name('บิลสั่งชื้อ ที่รอชำระ');
		}

		$loadTable->label(
			array(
				'userid' => 'ID ผู้สั่งชื้อ',
				'nameonbill' => 'ชื่อตัวแทน',
				'totalprice' => 'ราคารวม',
				'contactname' => 'ชื่อผู้ติดต่อ',
				'telephone' => 'เบอร์ติดต่อ',
				'status_pay' => 'สถานะการจ่าย'
			)
		);

		$loadTable->columns(
			array(
				'userid',
				'nameonbill',
				'totalprice',
				'contactname',
				'telephone',
				'status_pay'
			)
		);
		
		$loadTable->where('status_pay =',$is_pay);

		$loadTable->order_by('id','DESC');

		return $loadTable;
	}

}

?>
