<?php
class xcrud_user_model extends Model {

	public function useraccess($loadTable='',$agent=false)
	{
		$loadTable->table('user_system');
		$loadTable->table_name('ผู้ใช้ระบบ');
		
		if($agent == 'agent'){
			$loadTable->columns('username,lock_user,create_by,create_date');
			$loadTable->unset_add()
					->unset_edit()
					->unset_view()
					->unset_remove();

			$loadTable->where('user_level =','agent');
		}else{
			$loadTable->columns('username,user_level,store_id,lock_user,create_by,create_date');
		}
		$loadTable->label(
			array(
				'username' => 'ชื่อผู้ใช้',
				'password' => 'รหัสเข้าใช้',
				'user_level' => 'ระดับเข้าถึง',
				'store_id' => 'คลังที่ดูแล',
				'lock_user' => 'สถานะ',
				'create_by' => 'สร้างโดย',
				'create_date' => 'สร้างเมื่อ'
			)
		);

		$loadTable->fields(
			array(
				'username',
				'password',
				'user_level',
				'store_id',
				'lock_user',
			)
		);

		$loadTable->relation('create_by','user_system','id','username');
		$loadTable->relation('store_id','storehub','id','name_th');
		$loadTable->change_type('password', 'password', 'md5', array('maxlength'=>10,'placeholder'=>'enter password'));
		$loadTable->pass_var('create_by', $_SESSION['user']['id'],'create');

		$loadTable->order_by('id','DESC');

		return $loadTable;
	}

}

?>
