<?php
class xcrud_product_model extends Model {

	public function productlist($loadTable='',$storeid=false)
	{
		$loadTable->table('productlist');
		$loadTable->table_name('รายการสินค้า');
		$loadTable->columns('image,name,category,date_update,barcode_qr');
		$loadTable->label(
			array(
				'image' => 'รูป',
				'name' => 'ชื่อ',
				'category' => 'หมวดหมูสินค้า',
				'date_update' => 'แก้ไขจล่าสุด',
				'barcode_qr' => 'รหัสสินค้า',
				'storeid' => 'คลังสินค้า',
				'detail' => 'รายละเอียดสินค้า'

			)
		);

		if($storeid !== false){
			if($storeid == 0 && $_SESSION['user']['user_level'] == 'superadmin'){

			}else{
				$loadTable->where('storeid =',$storeid);
			}
		}

		$loadTable->field_tooltip('image','ใส่รูปสินค้า รูปสินค้าตัวแทนจำหน่ายจะเห็นรูปนี้ด้วย และใช้ในบิลสินค้า');
		$loadTable->field_tooltip('storeid','เลือกว่าสินค้านี้เป็นของคลังสินค้าไหน ถ้าไม่เลือก จะมีเพียงผู้ดูแลที่จะมองเห็นสินค้านี้');
		$loadTable->field_tooltip('detail','รายละเอียดสินค้า จะแสดงให้ตัวแทนจำหน่ายเห็น เพื่อนำไปเป็นข้อมูลในการขาย');
		$loadTable->field_tooltip('barcode_qr','รหัสสินค้า 13 ตัวขึ้นไป (SKU,Barcode,QR Code)');
		$loadTable->validation_required('barcode_qr',13);
		$loadTable->validation_pattern('barcode_qr','numeric');
		$loadTable->fields(
			array(
				'name',
				'image',
				'barcode_qr',
				'category',
				'storeid',
				'detail'
			)
		);
		$loadTable->column_pattern('barcode_qr','<img src="/bar/barcode.php?code={barcode_qr}&encoding=EAN&scale=1&mode=png">');

		$loadTable->relation('category','category','id','name_th');
		$loadTable->relation('storeid','storehub','id','name_th');

		$loadTable->pass_var('date_update', date('Y-m-d H:i:s'));
		$loadTable->pass_var('dateadd',date('Y-m-d H:i:s'),'create');

		$loadTable->change_type('image','image','',array(
			'thumbs'=>array(
				array('width'=> 70, 'folder'=>'thumbs_small'),
				array('width'=> 250, 'folder'=>'thumbs_middle')
			)
		));

		$loadTable->order_by('date_update','DESC');

		return $loadTable;
	}

}

?>
