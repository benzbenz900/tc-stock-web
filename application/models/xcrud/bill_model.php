<?php
class xcrud_bill_model extends Model {

	public function billlist($loadTable='',$nameTable=false)
	{
		$loadTable->table('openbill');
		if($nameTable !== false){
			$loadTable->table_name($nameTable);
			$loadTable->button('billdetail.html/?addtobill={id}&nameonbill={nameonbill}','เพิ่มสินค้าลงในบิล','fas fa-plus-circle');
		}else{
			$loadTable->table_name('บิลสั่งชื้อ');
			if($_SESSION['user']['user_level'] == 'superadmin' || $_SESSION['user']['user_level'] == 'store'){
				if($_SESSION['user']['user_level'] == 'store'){
					$loadTable->unset_add();
				}
				$loadTable->button('billdetail.html/?addtobill={id}&nameonbill={nameonbill}','สินค้าในบิล','fas fa-plus-circle');
			}
		}

		$loadTable->label(
			array(
				'userid' => 'ID ผู้สั่งชื้อ',
				'nameonbill' => 'ชื่อตัวแทน',
				'address' => 'ที่อยู่',
				'taxidnumber' => 'เลขเสียภาษี',
				'totalprice' => 'ราคารวม',
				'contactname' => 'ชื่อผู้ติดต่อ',
				'emailuser' => 'อีเมลผู้ติดต่อ',
				'telephone' => 'เบอร์ติดต่อ',
				'status_pay' => 'สถานะการจ่าย',
				'openbilldate' => 'วันที่เปิดบิล',
				'pay_date' => 'วันที่จ่ายเงิน',
				'proof_payment' => 'หลักฐานการข่ายเงิน'
			)
		);

		$loadTable->field_tooltip('pay_date','ใส่งันที่ ที่ตัวแทนหรือลูกค้ชำระเงิน ถ้ายังไม่ได้ชำระ ปล่อยว่างไว้');
		$loadTable->field_tooltip('taxidnumber','เลขเสียภาษี ถ้าไม่มีให้ปล่อยว่างไว้');
		$loadTable->field_tooltip('proof_payment','หลักฐานการข่ายเงิน รูปสลิปการโอนเงิน ถ้ายังไม่ได้ชำระปล่อยว่างไว้');

		$loadTable->relation('userid','user_system','id','username','user_system.user_level = "agent"');

		$loadTable->fields(
			array(
				'userid',
				'nameonbill',
				'address',
				'taxidnumber',
				'contactname',
				'emailuser',
				'telephone',
				'status_pay',
				'openbilldate',
				'pay_date',
				'proof_payment'
			)
		);

		$loadTable->change_type('proof_payment','image','',array(
			'thumbs'=>array(
				array('width'=> 70, 'folder'=>'thumbs_small'),
				array('width'=> 250, 'folder'=>'thumbs_middle')
			)
		));

		$loadTable->columns(
			array(
				'userid',
				'nameonbill',
				'totalprice',
				'contactname',
				'telephone',
				'status_pay'
			)
		);

		$loadTable->order_by('id','DESC');

		return $loadTable;
	}

	public function billdetail($loadTable='')
	{
		$loadTable->table('openbill_detail');
		$loadTable->table_name('สินค้าในบิล ID:'.$_GET['addtobill'].' ('.$_GET['nameonbill'].')');
		if($_SESSION['user']['user_level'] == 'store'){
			$loadTable->unset_add();
		}
		$loadTable->columns(
			array(
				'id_product',
				'qty_piece',
				'price_piece',
				'total_price',
				'store_id',
				'barcode_qr'
			)
		);

		$loadTable->fields(
			array(
				'id_product',
				'qty_piece',
				'price_piece',
				'total_price'
			)
		);

		$loadTable->column_callback('store_id','store_name');
		$loadTable->column_callback('barcode_qr','product_barcode');
		
		$loadTable->change_type('total_price','price','',array('prefix'=>'¥'));
		$loadTable->change_type('price_piece','price','',array('prefix'=>'¥'));
		$loadTable->pass_var('id_bill',$_GET['addtobill'],'create');

		$loadTable->relation('id_bill','openbill','id','nameonbill');
		$loadTable->relation('id_product','productlist','id','name');

		$loadTable->sum('price_piece,total_price');

		$loadTable->order_by('id','DESC');

		return $loadTable;
	}

}

?>
