<?php
class xcrud_category_model extends Model {

	public function category($loadTable='')
	{
		$loadTable->table('category');
		$loadTable->table_name('หมวดหมู่สินค้า');
		$loadTable->columns('name_cn,name_en,name_th');
		$loadTable->label(
			array(
				'name_cn' => 'ชื่อจีน',
				'name_en' => 'ชื่ออังกฤษ',
				'name_th' => 'ชื่อไทย',

			)
		);

		$loadTable->order_by('id','DESC');

		return $loadTable;
	}

}

?>
