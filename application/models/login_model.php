<?php

class login_model extends Model {
	
	public function getLogin($username,$password)
	{
		$getLogin = $this->query('
			SELECT * FROM `user_system` WHERE
			username = ? AND password = ?',
			$username,$password
		)->fetchArray();
		return ($getLogin) ? $getLogin:null;
	}

	public function getLoginId($id)
	{
		$getLogin = $this->query('
			SELECT * FROM `user_system` WHERE
			id = ?',
			$id
		)->fetchArray();
		return ($getLogin) ? $getLogin:null;
	}

}

?>
